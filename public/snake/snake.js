let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");
let tileCount = 20;
let tileSize = 18;
let headX = 10;
let headY = 10;
let xvelocity = 0;
let yvelocity = 0;
let appleX = 5;
let appleY = 5;
let snakeparts = [];
let  taillength = 2;
let gameover;

class snakepart{
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

drawgame();

function drawgame() {
    let speed = 7;
    setTimeout(drawgame, 1000 / speed);
    if(gameover){
        return;
    }
    clearscreen();
    drawsnake();
    changesnakeposition();
    drawapple();
    checkcollision();
    game_over();
}

function clearscreen(){
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight);
}

function drawsnake() {
    ctx.fillStyle = "orange";
    ctx.fillRect(headX * tileCount, headY * tileCount, tileSize, tileSize)

    ctx.fillStyle="green";

    for(let i = 0; i<snakeparts.length;i++){
        let part = snakeparts[i];
        ctx.fillRect(part.x * tileCount, part.y * tileCount, tileSize, tileSize);
    }
    snakeparts.push(new snakepart(headX, headY));
    if(snakeparts.length > taillength){
        snakeparts.shift();
    }
}

buttons();

function buttons(){
    document.getElementById("up").addEventListener('click', buttonUp);
    function buttonUp(){
        if (yvelocity == 1) {
            return;
        }
        yvelocity = -1;
        xvelocity = 0;
    }

    document.getElementById("down").addEventListener('click', buttonDown);
    function buttonDown(){
        if (yvelocity == -1) {
            return;
        }
        yvelocity = 1;
        xvelocity = 0;
    }

    document.getElementById("left").addEventListener('click', buttonLeft);
    function buttonLeft(){
        if (xvelocity == 1) {
            return;
        }
        yvelocity = 0;
        xvelocity = -1;
    }

    document.getElementById("right").addEventListener('click', buttonright)
    function buttonright(){
        if (xvelocity == -1) {
            return;
        }
        yvelocity = 0;
        xvelocity = 1;
    }
}

document.body.addEventListener('keydown', keyDown);

    function keyDown() {
        if (event.key == "ArrowUp") {
            if (yvelocity == 1) {
                return;
            }
            yvelocity = -1;
            xvelocity = 0;
        }
        if (event.key == "ArrowDown") {
            if (yvelocity == -1) {
                return;
            }
            yvelocity = 1;
            xvelocity = 0;
        }
        if (event.key == "ArrowLeft") {
            if (xvelocity == 1) {
                return;
            }
            yvelocity = 0;
            xvelocity = -1;
        }
        if (event.key == "ArrowRight") {
            if (xvelocity == -1) {
                return;
            }
            yvelocity = 0;
            xvelocity = 1;
        }
    }

function changesnakeposition() {
    headX = headX + xvelocity;
    headY = headY + yvelocity;
}

function drawapple(){
    ctx.fillStyle = "red";
    ctx.fillRect(appleX*tileCount, appleY*tileCount, tileSize, tileSize);
}

function checkcollision(){
    if(appleX === headX && appleY === headY){
        appleX = Math.floor(Math.random()*tileCount);
        appleY = Math.floor(Math.random()*tileCount);
        taillength++;
    }
}

function game_over(){

        if(xvelocity === 0 && yvelocity === 0){
            return false;
        }

        if(headY < 0){
            gameover = true;
        }

        if(headY === 25){
            gameover = true;
        }

        if(headX < 0){
            gameover = true;
        }

        if(headX === 25){
            gameover = true;
        }

        for(let i = 0; i<snakeparts.length;i++){
            let part = snakeparts[i];
            if(part.x === headX && part.y === headY){
                gameover=true;
                break;
            }
        }

        if(gameover){
            xvelocity = 0;
            yvelocity = 0;

            ctx.fillStyle="blue";
            ctx.font="50px verdana";
            ctx.fillText("Game over", canvas.clientWidth/4, canvas.clientHeight/4);
        }
}

document.getElementById("reset").addEventListener('click', reset);
    function reset(){
        headY = 10;
        headX = 10;
        xvelocity = 0;
        yvelocity = 0;
        appleX = 5;
        appleY = 5;
        snakeparts = [];
        taillength = 2;
        gameover = false;
    }
