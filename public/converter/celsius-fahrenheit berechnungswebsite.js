document.querySelector("#celsius").addEventListener("keyup", convert);

function convert() {

    let celsius = document.getElementById("celsius").value;

    if(celsius == ""){
        document.getElementById("tofahrenheit").value = "";
    }

    celsius = parseInt(celsius, 10) || false;
    if (!celsius) {
        throw 'Given input is not a number';
    }

    let fahrenheit;
    fahrenheit = (celsius * 9 / 5) + 32;

    document.getElementById("tofahrenheit").value = fahrenheit;
}