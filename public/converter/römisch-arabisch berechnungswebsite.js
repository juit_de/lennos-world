document.querySelector("#römisch").addEventListener ("keyup", convert);

function convert() {

    let input = document.getElementById("römisch").value;

    if(typeof input !== "string"){
        throw 'Given input is not a string';
    }

    input = input.replace(/[^IVXLCDM]/g, '');



    let arabic = 0;

    while (input !== '') {
        //correct input//
        {
            if(input == ""){
                document.getElementById("arabisch").value = "";
            }

            // if(input.includes("IIII")){
            //     document.getElementById("römisch").value = input.replace("IIII", "IV");
            //     console.log(new Error().lineNumber);
            // }

            if(input.includes("VV")){
                document.getElementById("römisch").value = input.replace("VV", "X");
            }

            if(input.includes("XXXX")){
                document.getElementById("römisch").value = input.replace("XXXX", "XL");
            }

            if(input.includes("XLX")){
                document.getElementById("römisch").value = input.replace("XLX", "L");
            }

            if(input.includes("LXL")){
                document.getElementById("römisch").value = input.replace("LXL", "XC");
            }

            if(input.includes("LL")){
                document.getElementById("römisch").value = input.replace("LL", "C");
            }

            if(input.includes("XCX")){
                document.getElementById("römisch").value = input.replace("XCX", "C");
            }
        }


        //convert//
        {
            if (input.includes('I') && !input.includes('IV') && !input.includes('IX')) {
                input = input.replace('I', '');
                arabic++;

                console.log(new Error().lineNumber);
            }

            if (input.includes('IV')) {
                input = input.replace('IV', '');
                arabic = arabic + 4;
                console.log(new Error().lineNumber);
            }

            if (input.includes('V')) {
                input = input.replace('V', '');
                arabic = arabic + 5;

                console.log(new Error().lineNumber);
            }

            if (input.includes('IX')) {
                input = input.replace('IX', '');
                arabic = arabic + 9;

                console.log(new Error().lineNumber);
            }

            if (input.includes('X') && !input.includes('XL') && !input.includes('XC')) {
                input = input.replace('X', '');
                arabic = arabic + 10;
            }

            if (input.includes('L') && !input.includes('XL')) {
                input = input.replace('L', '');
                arabic = arabic + 50;
            }

            if (input.includes('XL')) {
                input = input.replace('XL', '');
                arabic = arabic + 40;
            }

            if (input.includes('C') && !input.includes('XC') && !input.includes('CD') && !input.includes('CM')) {
                input = input.replace('C', '');
                arabic = arabic + 100;
            }

            if (input.includes('XC')) {
                input = input.replace('XC', '');
                arabic = arabic + 90;
            }

            if (input.includes('D') && !input.includes('CD')) {
                input = input.replace('D', '');
                arabic = arabic + 500;
            }

            if (input.includes('CD')) {
                input = input.replace('CD', '');
                arabic = arabic + 400;
            }

            if (input.includes('M')) {
                input = input.replace('M', '')
                arabic = arabic + 1000;
            }

            if (input.includes('CM')) {
                input = input.replace('CM', '')
                arabic = arabic + 900;
            }
        }

    }
    document.getElementById("arabisch").textContent = arabic;
}