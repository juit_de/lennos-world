document.querySelector("#arabic").addEventListener("keyup", convert);

function convert() {

    let arabic = document.getElementById("arabic").value;

    arabic = parseInt(arabic, 10) || false;

    if(arabic == ""){
        document.getElementById("roman").textContent = "";
        return;
    }

    if (!arabic) {
        throw 'Given input is not a number';
    }

    let roman1 = '';
    let roman2 = '';
    let roman3 = '';
    let roman4 = '';
    let roman;
    let counter = 0;
    let hundredcounter = 0;
    let thousandcounter = 0;

    while (arabic >= 1000) {
        arabic = arabic - 1000;
        thousandcounter = thousandcounter + 1000;
    }
    while (arabic >= 100) {
        arabic = arabic - 100;
        hundredcounter = hundredcounter + 100;
    }
    while (arabic >= 10) {
        arabic = arabic - 10;
        counter = counter + 10;
    }
    if (arabic >= 5) {
        roman1 = 'V' + 'I'.repeat(arabic - 5);
    }
    if (arabic === 4) {
        roman1 = 'IV';
    }
    if (arabic < 4) {
        roman1 = 'I'.repeat(arabic);
    }
    if (arabic === 9) {
        roman1 = 'IX';
    }
    if (counter === 10) {
        roman2 = 'X';
    }
    if (counter > 10 && counter < 40) {
        roman2 = 'X'.repeat(counter / 10);
    }
    if (counter >= 40 && counter < 50) {
        roman2 = 'XL';
    }
    if (counter >= 50 && counter < 90) {
        roman2 = 'L' + 'X'.repeat((counter - 50) / 10);
    }
    if (counter >= 90 && counter < 100) {
        roman2 = 'XC';
    }
    if (hundredcounter >= 100 && hundredcounter < 400) {
        roman3 = 'C'.repeat(hundredcounter / 100);
    }
    if (hundredcounter >= 400 && hundredcounter < 500) {
        roman3 = 'CD';
    }
    if (hundredcounter >= 500 && hundredcounter < 900) {
        roman3 = 'D' + 'C'.repeat((hundredcounter - 500) / 100);
    }
    if (hundredcounter >= 900 && hundredcounter < 1000) {
        roman3 = 'CM';
    }
    if (thousandcounter >= 1000) {
        roman4 = 'M' + 'M'.repeat((thousandcounter - 1000) / 1000);
    }
    roman = roman4 + roman3 + roman2 + roman1;

    document.getElementById("roman").textContent = roman;
}