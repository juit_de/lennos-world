document.querySelector("#fahrenheit").addEventListener("keyup", convert);

function convert() {
    let fahrenheit = document.getElementById("fahrenheit").value;

    if(fahrenheit == ""){
        document.getElementById("tocelsius").value = "";
    }

    fahrenheit = parseInt(fahrenheit, 10) || false;
    if (!fahrenheit) {
        throw 'Given input is not a number';
    }

    let celsius
    celsius = (fahrenheit - 32) * 5/9

    document.getElementById("tocelsius").value = celsius;
}