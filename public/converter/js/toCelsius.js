export function toCelsius(fahrenheit) {
    fahrenheit = parseInt(fahrenheit, 10) || false;
    if (!fahrenheit) {
        throw 'Given input is not a number';
    }

    let celsius
    celsius = (fahrenheit - 32) * 5/9

    return celsius;
}