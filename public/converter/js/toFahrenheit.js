export function toFahrenheit(celsius) {
    celsius = parseInt(celsius, 10) || false;
    if (!celsius) {
        throw 'Given input is not a number';
    }

    let fahrenheit;
    fahrenheit = (celsius * 9 / 5) + 32;

    return fahrenheit;
}