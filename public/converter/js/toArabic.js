export function toArabic(roman) {

    let input = roman;

    if(typeof input !== "string"){
        throw 'Given input is not a string';
    }

    input = input.replace(/[^IVXLCDM]/g, '');

    let arabic = 0;

    while (input !== '') {

        if (input.includes('I') && !input.includes('IV') && !input.includes('IX')) {
            input = input.replace('I', '');
            arabic++;
        }

        if (input.includes('IV')) {
            input = input.replace('IV', '');
            arabic = arabic + 4;
        }

        if (input.includes('V')) {
            input = input.replace('V', '');
            arabic = arabic + 5;
        }

        if (input.includes('IX')) {
            input = input.replace('IX', '');
            arabic = arabic + 9;
        }

        if (input.includes('X') && !input.includes('XL') && !input.includes('XC')) {
            input = input.replace('X', '');
            arabic = arabic + 10;
        }

        if (input.includes('L') && !input.includes('XL')) {
            input = input.replace('L', '');
            arabic = arabic + 50;
        }

        if (input.includes('XL')) {
            input = input.replace('XL', '');
            arabic = arabic + 40;
        }

        if (input.includes('C') && !input.includes('XC') && !input.includes('CD') && !input.includes('CM')) {
            input = input.replace('C', '');
            arabic = arabic + 100;
        }

        if (input.includes('XC')) {
            input = input.replace('XC', '');
            arabic = arabic + 90;
        }

        if (input.includes('D') && !input.includes('CD')) {
            input = input.replace('D', '');
            arabic = arabic + 500;
        }

        if (input.includes('CD')) {
            input = input.replace('CD', '');
            arabic = arabic + 400;
        }

        if (input.includes('M')) {
            input = input.replace('M', '')
            arabic = arabic + 1000;
        }

        if (input.includes('CM')) {
            input = input.replace('CM', '')
            arabic = arabic + 900;
        }
    }
    return arabic;
}