import {
    toRoman as toRoman
} from './toRoman';

import {
    toCelsius as toCelsius
} from './toCelsius';

import {
    toFahrenheit as toFahrenheit
} from './toFahrenheit';

import {
    toArabic as toArabic
} from './toArabic';

export function convert() {
    return {
        toRoman: (arabic) => toRoman(arabic),
        toCelsius: (fahrenheit) => toCelsius(fahrenheit),
        toFahrenheit: (celsius) => toFahrenheit(celsius),
        toArabic: (roman) => toArabic(roman)
    };
}

export default convert;