let canvas = document.getElementById("canvas");
let ctx = canvas.getContext("2d");
let xtileCount = 56;
let ytilecount = 39;
let tileSize = 18;
let headX = 12;
let headY = 17;
let secondheadx = 40;
let secondheady = 17;
let xvelocity = 0;
let yvelocity = 0;
let secondxvelocity = 0;
let secondyvelocity = 0
let appleX;
let appleY;
let snakeparts = [];
let secondsnakeparts = [];
let taillength = 2;
let secondtaillength = 2;
let lengthperapple = 1;
let blue_wins;
let Green_wins;
let wall = false;
let wallx;
let wally;
let direction;
let wallLength;
let wallparts = [];
let moveright = Math.random() < 0.5;
let moveup = Math.random() < 0.5;
let speed = 7;
spawnapple();

class snakepart {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class secondsnakepart {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}

class wallpart {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
}
function morespeed(){
    if(speed >= 60){
        return;
    }
    setTimeout( function (){
        speed *= 1.03;
        morespeed();
    }, 1000);
}
morespeed();
buildwall();
drawgame();

function drawgame() {
    setTimeout(drawgame, 1000 / speed);
    if (blue_wins || Green_wins) {
        return;
    }
    clearscreen();
    drawsnake();
    drawsnake2();
    changesnakeposition();
    changesecondsnakeposition();
    drawapple();
    checkcollision();
    drawwall();
    movewall();
    game_over_green();
    game_over_blue();
    gpa();
}

function gpa() {
    let growth = document.getElementById("growthperapple");
    growth.addEventListener("change", function () {
        lengthperapple = parseInt(growth.value);
        document.getElementById("growthoutput").value = lengthperapple;
    });
}

function clearscreen() {
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.clientWidth, canvas.clientHeight);
}

function drawsnake() {
    ctx.fillStyle = "orange";
    ctx.fillRect(headX * tileSize, headY * tileSize, tileSize, tileSize)

    ctx.fillStyle = "green";

    for (let i = 0; i < snakeparts.length; i++) {
        let part = snakeparts[i];
        ctx.fillRect(part.x * tileSize, part.y * tileSize, tileSize, tileSize);
    }
    snakeparts.push(new snakepart(headX, headY));
    if (snakeparts.length > taillength) {
        snakeparts.shift();
    }
}

function drawsnake2() {
    ctx.fillStyle = "yellow";
    ctx.fillRect(secondheadx * tileSize, secondheady * tileSize, tileSize, tileSize);

    ctx.fillStyle = "blue";

    for (let i = 0; i < secondsnakeparts.length; i++) {
        let part = secondsnakeparts[i];
        ctx.fillRect(part.x * tileSize, part.y * tileSize, tileSize, tileSize);
    }
    secondsnakeparts.push(new secondsnakepart(secondheadx, secondheady));
    if (secondsnakeparts.length > secondtaillength) {
        secondsnakeparts.shift();
    }
}

document.body.addEventListener('keydown', keyDown);

function keyDown() {
    if (event.key == "ArrowUp") {
        if (yvelocity == 1) {
            return;
        }
        yvelocity = -1;
        xvelocity = 0;
    }
    if (event.key == "ArrowDown") {
        if (yvelocity == -1) {
            return;
        }
        yvelocity = 1;
        xvelocity = 0;
    }
    if (event.key == "ArrowLeft") {
        if (xvelocity == 1) {
            return;
        }
        yvelocity = 0;
        xvelocity = -1;
    }
    if (event.key == "ArrowRight") {
        if (xvelocity == -1) {
            return;
        }
        yvelocity = 0;
        xvelocity = 1;
    }
}


document.body.addEventListener("keydown", function () {
    if (event.key == "w") {
        if (secondyvelocity === 1) {
            return;
        }
        secondyvelocity = -1;
        secondxvelocity = 0;
    }
    if (event.key == "a") {
        if (secondxvelocity == 1) {
            return;
        }
        secondyvelocity = 0;
        secondxvelocity = -1;
    }
    if (event.key == "s") {
        if (secondyvelocity == -1) {
            return;
        }
        secondyvelocity = 1;
        secondxvelocity = 0;
    }
    if (event.key == "d") {
        if (secondxvelocity == -1) {
            return;
        }
        secondyvelocity = 0;
        secondxvelocity = 1;
    }
});

function changesnakeposition() {
    headX = headX + xvelocity;
    headY = headY + yvelocity;
}

function changesecondsnakeposition() {
    secondheadx = secondheadx + secondxvelocity;
    secondheady = secondheady + secondyvelocity;
}


function drawapple() {
    ctx.fillStyle = "red";
    ctx.fillRect(appleX * tileSize, appleY * tileSize, tileSize, tileSize);
}

function checkcollision() {
    if (appleX === headX && appleY === headY) {
        spawnapple();
        taillength += lengthperapple;
    }

    if (appleX === secondheadx && appleY === secondheady) {
        appleX = Math.floor(Math.random() * xtileCount);
        appleY = Math.floor(Math.random() * ytilecount);
        secondtaillength += lengthperapple;
    }
}

function spawnapple() {
    let collision;
    do {
        collision = false;
        appleX = Math.floor(Math.random() * xtileCount);
        appleY = Math.floor(Math.random() * ytilecount);

        for (let i = 0; i < snakeparts.length; i++) {
            let part = snakeparts[i];
            if (appleX === part.x && appleY === part.y) {
                collision = true;
            }
        }

        for (let i = 0; i < secondsnakeparts.length; i++) {
            if (appleX === secondsnakeparts[i].x && appleY === secondsnakeparts[i].y) {
                collision = true;
            }
        }

        for (let i = 0; i < wallparts.length; i++) {
            if (appleX === wallparts[i].x && appleY === wallparts[i].y) {
                collision = true;
            }
        }
    } while (collision);
}

function drawwall() {
    ctx.fillStyle = "white";
    ctx.fillRect(wallx * tileSize, wally * tileSize, tileSize, tileSize);

    for (let i = 0; i < wallparts.length; i++) {
        let part = wallparts[i];
        ctx.fillRect(part.x * tileSize, part.y * tileSize, tileSize, tileSize);
    }
}

function buildwall() {
    wallLength = 5;
    wallx = Math.floor(Math.random() * (xtileCount - wallLength));
    wally = Math.floor(Math.random() * (ytilecount - wallLength));
    direction = Math.round(Math.random() + 1);

    wallparts = [];
    for (let i = 0; i < wallLength; i++) {
        let x = wallx;
        let y = wally;
        if (direction === 1) {
            x += i;
        }
        if (direction === 2) {
            y += i;
        }

        wallparts.push(new wallpart(x, y));
    }
}

function movewall() {
    //Horizontal:
    if (direction === 1) {
        if (wallx === xtileCount - wallLength) {
            moveright = false;
        }
        if (wallx === 0) {
            moveright = true;
        }
        if(wally === xtileCount - wallLength){
            moveup = false;
        }
        if (moveright) {
            wallx++;
            for (let i = 0; i < wallparts.length; i++) {
                wallparts[i].x += 1;
            }
        } else {
            wallx--;
            for (let i = 0; i < wallparts.length; i++) {
                wallparts[i].x -= 1;
            }
        }
    }
    //Vertikal:
    if (direction === 2) {
        if (wally === ytilecount - wallLength) {
            moveup = false;
        }
        if (wally === 0) {
            moveup = true;
        }
        if (moveup) {
            wally++;
            for (let i = 0; i < wallparts.length; i++) {
                wallparts[i].y += 1;
            }
        } else {
            wally--;
            for (let i = 0; i < wallparts.length; i++) {
                wallparts[i].y -= 1;
            }
        }
    }
}

function game_over_green() {

    if (xvelocity === 0 && yvelocity === 0) {
        return false;
    }

    if (headY < 0) {
        headY = ytilecount - 1;
        return;
    }

    if (headY === ytilecount - 1) {
        headY = 0;
        return;
    }

    if (headX < 0) {
        headX = xtileCount - 1;
        return;
    }

    if (headX >= xtileCount - 1) {
        headX = 0;
        return;
    }

    for(let i = 0; i<snakeparts.length; i++){
        for (let j = 0;j< wallparts.length; j++) {
            if (snakeparts[i].x === wallparts[j].x && snakeparts[i].y === wallparts[j].y) {
                blue_wins = true;
                break;
            }
        }
    }

    for (let i = 0; i < snakeparts.length; i++) {
        let part = snakeparts[i];
        if (part.x === headX && part.y === headY) {
            blue_wins = true;
            break;
        }
    }

    for (let i = 0; i < secondsnakeparts.length; i++) {
        let part = secondsnakeparts[i];
        if (part.x === headX && part.y === headY) {
            blue_wins = true;
            break;
        }
    }

    if (blue_wins) {
        xvelocity = 0;
        yvelocity = 0;
        secondxvelocity = 0;
        secondyvelocity = 0;

        ctx.fillStyle = "blue";
        ctx.font = "50px verdana";
        ctx.fillText("Blue wins", 350, 200);
    }
}

function game_over_blue() {

    if (secondxvelocity === 0 && secondyvelocity === 0) {
        return false;
    }

    if (secondheady < 0) {
        secondheady = ytilecount - 1;
        return;
    }

    if (secondheady >= ytilecount - 1) {
        secondheady = 0;
        return;
    }

    if (secondheadx < 0) {
        secondheadx = xtileCount - 1;
        return;
    }

    if (secondheadx >= xtileCount - 1) {
        secondheadx = 0;
        return;
    }

    for(let j = 0; j<secondsnakeparts.length; j++){
        for (let i = 0; i < wallparts.length; i++) {
            if (secondsnakeparts[j].x === wallparts[i].x && wallparts[i].y === secondsnakeparts[j].y) {
                Green_wins = true;
                break;
            }
        }
    }

    for (let i = 0; i < secondsnakeparts.length; i++) {
        let part = secondsnakeparts[i];
        if (part.x === secondheadx && part.y === secondheady) {
            Green_wins = true;
            break;
        }
    }

    for (let i = 0; i < snakeparts.length; i++) {
        let part = snakeparts[i];
        if (part.x === secondheadx && part.y === secondheady) {
            Green_wins = true;
            break;
        }
    }


    if (Green_wins) {
        secondxvelocity = 0;
        secondyvelocity = 0;
        xvelocity = 0;
        yvelocity = 0;

        ctx.fillStyle = "green";
        ctx.font = "50px verdana";
        ctx.fillText("Green wins", 350, 200)
    }
}

document.body.addEventListener('keydown', reset);

function reset() {
    if (event.key == "r") {
        headY = 17;
        headX = 12;
        secondheadx = 40;
        secondheady = 17;
        xvelocity = 0;
        yvelocity = 0;
        secondxvelocity = 0;
        secondyvelocity = 0;
        spawnapple();
        snakeparts = [];
        taillength = 2;
        secondsnakeparts = [];
        secondtaillength = 2;
        wall = false;
        blue_wins = false;
        Green_wins = false;
        buildwall();
        speed = 7;
        morespeed();
    }
}